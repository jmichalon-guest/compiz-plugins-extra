Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: compiz-plugins-extra
Source: https://github.com/compiz-reloaded/compiz-plugins-extra

Files: *
Copyright: (Lead Developers, see AUTHORS for full list): 
	   2007 Danny Baumann <dannybaumann@web.de>
	   David Reveman <davidr@novell.com>
	   Dennis Kasprzyk <onestone@opencompositing.org>
	   Erkin Bahceci <erkinbah@gmail.com>
	   Guillaume Seguin - iXce <guillaume@segu.in>
	   Kristian Lyngstøl <kristian@bohemians.org>
           2015 Wolfgang Ulbrich <chat-to-me@raveit.de>
           2015 Sorokin Alexei <sor.alexei@meowr.ru>
           2017 Scott Moreau <oreaus@gmail.com>
License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in /usr/share/common-licenses/GPL-2.

Files: src/showdesktop/showdesktop.c
Copyright: 2007 Diogo "playerX" Ferreira
License: misc-1
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 Give credit where credit is due, keep the authors message below.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: debian/*
Copyright: 2007 Sean Finney <seanius@debian.org>
           2016 Jof Thibaut <compiz@tuxfamily.org>
License: GPL-3
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.
